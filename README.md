# Neural Coding of Social Memory in the TgF344-AD Transgenic Rat Model of Alzheimers Disease



Implementation of SWR detector, calculation of energy of specific frequency bands of LFP signal and frequency coupling expressed by Phase-locking value, Mean Vector length and Modulation Index.



## SWR_detection - implementation of SWR detection
 main function for SWR detection. The main script is final_detector.m
#### input:
       signal              signal for SWR detection
       fs                  sampling frequency
       HIL_thr             Hilbert detector threshold (default 5)
       SLL_thr             Shot-line Length detector threshold (default 97.5)
       freqBand            Frequencz band of interest
       Filter_window       filter window size in ms (default 0.05)
       epochLength         epoch size in ms (default 180)
#### output:   
       id_new_WV           indexes of centers of SWRs

## energy - impementation of calculation of energy of specific frequency bands
 main function for calculation of energy in specific freqeuncy bands, whole PSD via welch method and spectrogram. User can set the time range of interest
#### input:
       data1           - data stracture of signal
       data1.values    - signal
       data1.times     - time vector of signal
       norm_type       - set normalization on or off (norm_type = 0 -> on | norm_type = 0 -> off)
       F_c             - array which describe frequency bands:
                       'Delta','Theta','Alpha','Beta', 'Low Gamma', 'High Gamma'
       T               - set start and end of ROIs in sec | possible double format (1.25s)
                           Example:                                                  
                               T = [a1 b1; a2 b2; ... ;ax bx]
                               Selects time intervals:
                               t1: a1-b1 | t2: a2-b2 | t3: a3-b3
                               where a"x" is start time of interval t"x"
                               b"x" is end time of interval t"x"
       F_h             - Max frequency of interest
       spec_win_size   - Window size for spectrogram
       nvrlap          - Percentage of window overlaping
       spec_F_range    - Set range of y axis (frequency) for spectrograms
       k_cax           - Define range of colorbar for spectrograms
       chann_name      - Define name of channel
       fs1             - Define sampling frequency

#### output:
       F_BP_PSD1(1,:)  - means of PSD in frequency bands for first signal   
       F_BP_PSD1(2,:)  - MSE of PSD in frequency bands for first signal
       PSD(1,:)        - PSD of first signal
       FREQs           - Frequency axis for PSDs and Cxy
       t1              - time vector of signal
       SPECTOGRAM_VALS - Structure with spectrograms for defined time slices

## PAC - implementation of frequency coupling calculation   	
     main fucntion for calculation phase-locking value, mean vector length and modulation index for whole signal (output PAC) 
     and also calculation of PAC in time windows for extracting the dependence of PAC in time(output PAC_sliding).
     User can set the time range of interest.
####     input:
           signal          - signal for calculation
           t               - time vector of signal
           T               - time range of interest
           fs              - sampling frequency of original_signal
           phase_freq_band - array [a b] (range) for phase carrying freq band 
           Amp_freq_band   - array [a b] (range) for amplitude carrying freq band
           n_bins          - number of bins for MI calculation (18 is commonly used)
           sgtit_time      - time range of interest
           windowSize      - window size of sliding PAC
           overlap         - overlap of windows in sliding PAC
           channel_name    - channel name   
####     Output:
           PAC             - structer with calculated PLV,MVL and MI
           PAC_sliding     - structure with calculated PLV,MVL and MI in
                             sliding window
