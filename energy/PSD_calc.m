function [F_BP_PSD1,PSD,FREQs,t1,SPECTOGRAM_VALS]=PSD_calc( ...
    data1,norm_type,F_c,T1,F_h,spec_win_size,nvrlap,spec_F_range,k_cax,chann_name,fs1)
% main function for calculation of coherence and power spectral density
%
% input:
%       data1           - data stracture of signal
%       data1.values    - signal
%       data1.times     - time vector of signal
%       norm_type       - set normalization on or off (norm_type = 0 -> on | norm_type = 0 -> off)
%       F_c             - array which describe frequency bands:
%                       'Delta','Theta','Alpha','Beta', 'Low Gamma', 'High Gamma'
%       T               - set start and end of ROIs in sec | possible double format (1.25s)
%                           Example:                                                  
%                               T = [a1 b1; a2 b2; ... ;ax bx]
%                               Selects time intervals:
%                               t1: a1-b1 | t2: a2-b2 | t3: a3-b3
%                               where a"x" is start time of interval t"x"
%                               b"x" is end time of interval t"x"
%       F_h             - Max frequency of interest
%       spec_win_size   - Window size for spectrogram
%       nvrlap          - Percentage of window overlaping
%       spec_F_range    - Set range of y axis (frequency) for spectrograms
%       k_cax           - Define range of colorbar for spectrograms
%       chann_name      - Define name of channel
%       fs1             - Define sampling frequency
%
% output:
%       F_BP_PSD1(1,:)  - means of PSD in frequency bands for first signal   
%       F_BP_PSD1(2,:)  - MSE of PSD in frequency bands for first signal
%       PSD(1,:)        - PSD of first signal
%       FREQs           - Frequency axis for PSDs and Cxy
%       t1              - time vector of signal
%       SPECTOGRAM_VALS - Structure with spectrograms for defined time slices

chann_name = removeInvalidStructureCharacters(chann_name);

F_h = F_h+51;
origin1 = data1.values;
t1 = data1.times;


% fs1 = round(length(origin1)/t1(end));

            min_freq = F_c(1, 1);
            cycles = 4;

            % Calculate the minimum required FFT size (nfft) for this band
            nfft1 = ceil(fs1 * (1/min_freq * cycles));

            % Find the next power of 2 for nfft1
            nfft1 = 2^nextpow2(nfft1)*2;

            % Calculate the overlap based on nfft (e.g., 50% overlap)
            overlap1 = nfft1 / 2;

            % Generate the Hanning window of size nfft
            window1 = hanning(nfft1);

%% 50Hz AND HARMONICS NOTCH FILT + NORMALIZATION
% 50Hz filtration
sig_filt1 = notch_eeg(origin1, fs1,F_h);
% hum_fs = 50;
% f_max = F_h;
% sig_filt1 = notch_eeg(origin1, fs1,hum_fs,f_max);


%%
if norm_type == 0
    normalized_EEG_data1 = sig_filt1;
    fprintf('Data was not normalized\n')

elseif norm_type == 1
    % z-score normalization
    mean_EEG_data1 = mean(sig_filt1);
    std_EEG_data1 = std(sig_filt1);
    normalized_EEG_data1 = (sig_filt1 - mean_EEG_data1)./std_EEG_data1;
    
    fprintf('Data was normalized by substracting its mean and divided by its std\n')
end




spec_win = hamming(spec_win_size);
spec_nover = round((nvrlap/100)*spec_win_size);


[normalized_EEG_data1,t1,Spectrogram_values1] = time_slicing(normalized_EEG_data1,t1,T1,fs1,spec_win,spec_nover,chann_name,spec_F_range,k_cax);
SPECTOGRAM_VALS = Spectrogram_values1;

maxF = max(max(F_c));




%% BUTTER BANDPASS FILTRATION
%set order of bandpass filter 
% order = 2;
% [SIGs_BP1] = bp_fil(normalized_EEG_data1, F_c, fs1, order);
SIGs_BP1 = normalized_EEG_data1;
%% POWER SPECTRA AND COHERENCE COMPUTATION
% cycles=4;                 % at least 4 sines are visible
% nfft1=(nextpow2(min([length(normalized_EEG_data1)])/cycles))-1; 
% nfft1=2^nfft1;            % uses nfft sampling points to calculate the discrete Fourier transform.
% overlap1 = nfft1/2;       % uses noverlap samples of overlap between adjoining segments.
% window1 = hanning(nfft1); % hanning is default



% Unique function for PSDs and MSCOH calculation
[PSDs1,FREQs] = PSDx(SIGs_BP1,window1,overlap1,nfft1,fs1);

%% MEANING OVER FREQUENCY BANDS
[BP_mns1,~,Errs_PSD1] = band_mean(PSDs1,FREQs,F_c);

F_BP_PSD1(1,:) = BP_mns1;
F_BP_PSD1(2,:) = Errs_PSD1;

%% Sepctrum Calculation (whole unfiltered signal)


PSD(1,:) = PSDs1;


%% PLOT POWER SPECTRUMS AND COHERENCE FOR WHOLE SIGNAL
% Spectrums
figure
subplot(211)
plot(t1,normalized_EEG_data1)

subplot(212)
plot(FREQs,PSDs1)

grid on
title(strcat('Power specrtum of: ',chann_name))
xlabel('frequency [Hz]'); ylabel('Power [W/Hz]')
xlim([0 maxF+20])
% legend('signal1')



%% PLOT MEANS OF POWER SPECTRUM AS BARCHART
% Data
y1 = BP_mns1';  
err1 = Errs_PSD1';

% Plot
figure 
hb = bar(y1, 'BarWidth', 1); % get the bar handles
hold on;
for k = 1:size(y1,2)
    % get x positions per group
    xpos = hb(k).XData + hb(k).XOffset;
    % draw errorbar
    errorbar(xpos, y1(:,k), err1(:,k), 'LineStyle', 'none', ... 
        'Color', 'k', 'LineWidth', 2);
end

% Set Axis properties
set(gca,'xticklabel',{'Delta','Theta','Alpha','Beta', 'Low Gamma', 'High Gamma'});
ylabel('Energy [mV^{2}/Hz]')
title(strcat('Energy of frequency bands of rat and channel: ',chann_name))
% title(strcat('Energy of frequency bands'))

legend('Energy', 'SE')
grid on
hold off

end


function [BP_mns,Xs,Errs] = band_mean(SPCTRs,FREQs,bands)
%   Function for extract means of PSD and MSCOH for specific frequency
%   bands

    for i = 1:size(bands,1)

        ix_L = find(FREQs>bands(i,1),1);
        ix_H = find(FREQs>bands(i,2),1);

        SPCTRs_band = SPCTRs(ix_L:ix_H);
        spctrs_band_mean = mean(SPCTRs_band);
        BP_mns(i) = spctrs_band_mean;
        Errs(i) = std(SPCTRs_band)/length(SPCTRs_band);

        x_ax = zeros(size(SPCTRs));
        x_ax(ix_L:ix_H) =  1;
        Xs(i,:) = x_ax*spctrs_band_mean;%*(1000/(i^2));
    end


end

function [PSDs1,FREQs] = PSDx(SIGs1,window,noverlap,nfft,fs)

        if length(SIGs1) < length(window)


         
            nfft = 2^13;
            


            % Calculate the overlap based on nfft (e.g., 50% overlap)
            noverlap = round(nfft / 2);

            % Generate the Hanning window of size nfft
            window = hanning(nfft);
        end
        [Sxx,freq1]=pwelch(SIGs1, window, noverlap, nfft, fs);                 % estimate Sxx
        PSDs1 = Sxx;
        FREQs = freq1;

end
function [s1_sl,t1_sl,Spectrogram_values] = time_slicing(s1_or,t1,T,fs1,window,noverlap,sig_name,spec_F_range,k_cax)
    s1_sl = [];
    t1_sl = [];

    
    for i = 1:size(T,1)
        id_st = round(T(i,1)*fs1 - floor(t1(1)*fs1));
        id_end = round(T(i,2)*fs1- ceil(t1(1)*fs1));
        if id_st == 0
            id_st = 1;
        elseif id_st < 0
            error('Error: start time of interval must be grater value than zero')
        elseif id_end < 0
            error('Error: end time of interval must be grater value than zero')
        elseif id_end <= id_st
            error('Error: start time must be smaller value than end time')
        end

        if id_st < 1
            id_st = 1;
        end

        if id_end > length(s1_or)
            id_end = length(s1_or);
        end

        s1_slice = s1_or(id_st:id_end);
        t1_slice = t1(id_st:id_end);
        
        s1_sl = [s1_sl; s1_slice];
        t1_sl = [t1_sl; t1_slice];

        nfft = length(window);
        
        % Compute and plot the spectrogram
        [S,F,t_ax,P] = spectrogram(s1_sl, window, noverlap, nfft, fs1, 'yaxis');
        spec_t_ax = linspace(T(i,1),T(i,2),length(t_ax));
        
        % Find indices of F that fall within your desired range
        idx = F <= spec_F_range(2); 
        % Restrict S, F, and P to your desired range
        S = S(idx, :);
        F = F(idx);
        P = P(idx, :);

        idx = F >= spec_F_range(1); 
        % Restrict S, F, and P to your desired range
        S = S(idx, :);
        F = F(idx);
        P = P(idx, :);
        
        figure
        imagesc(spec_t_ax,F,10*log10(P))
        % Get the current color axis limits
        caxis_min = min(10*log10(P(:)));
        caxis_max = max(10*log10(P(:)));
        
        % Set the color axis limits
        C_min = min(caxis_min/k_cax, caxis_max-10);
        caxis([C_min caxis_max]);
        set(gca,'YDir','normal')
        xlabel('Time (s)')
        ylabel('Frequency (Hz)')
        colorbar
%         ylim(spec_F_range)
        tit_name = strcat(sig_name,' time slice: ',num2str(T(i,1)), '-',num2str(T(i,2)),'s');
        title(tit_name)
        colormap jet
        Ss{i} = S;
        Fs{i} = F;
        Spec_ts{i} = t_ax;
        Ps{i} = P;
    end
    Spectrogram_values.S = Ss;
    Spectrogram_values.F = Fs;
    Spectrogram_values.t_ax = Spec_ts;
    Spectrogram_values.P = Ps;



end

function eeg_data = notch_eeg(eeg_data, fs,F_h)
% tic
% hum_fs = 50;
%     eeg_data = filt50test(eeg_data, fs,hum_fs,F_h);
%    toc

%    tic
    max_har = floor(((F_h-1))/50);
    F_filt = 50;
    figure
    for i = 1:max_har
        F_filt_actual = F_filt*i;
        d = designfilt('bandstopiir','FilterOrder',20, ...
                   'HalfPowerFrequency1',F_filt_actual-1,'HalfPowerFrequency2',F_filt_actual+1, ...
                   'DesignMethod','butter','SampleRate',fs);
        eeg_data = filtfilt(d,eeg_data);
         
%         [h,w] = freqz(d,2^12);
%         f=[w*fs]/(2*pi);
%         plot(f,10*log10(abs(h)))
%         hold on
       
    end
%     hold off
%     xlim([0 120])
% 
%     toc

end

function newStr = removeInvalidStructureCharacters(str)
    % Invalid characters for structure names
    invalidCharacters = ['_'];
    
    % Remove invalid characters from the string
    newStr = str;
    for i = 1:length(invalidCharacters)
        newStr = strrep(newStr, invalidCharacters(i), ' ');
    end
end


