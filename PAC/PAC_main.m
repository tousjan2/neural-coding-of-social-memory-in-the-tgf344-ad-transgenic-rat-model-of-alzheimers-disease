function [PAC,PAC_sliding] = PAC_main(signal,t,T,fs, phase_freq_band, Amp_freq_band, n_bins,windowSize,overlap,channel_name)
    % main fucntion for calculation phase-locking value, mean vector length
    % and modulation index
    % input:
    %       signal          - signal for calculation
    %       t               - time vector of signal
    %       T               - time range of interest
    %       fs              - sampling frequency of original_signal
    %       phase_freq_band - array [a b] (range) for phase carrying freq band 
    %       Amp_freq_band   - array [a b] (range) for amplitude carrying freq band
    %       n_bins          - number of bins for MI calculation (18 is commonly used)
    %       sgtit_time      - time range of interest
    %       windowSize      - window size of sliding PAC
    %       overlap         - overlap of windows in sliding PAC
    %       channel_name    - channel name
    %
    % Output:
    %       PAC             - structer with calculated PLV,MVL and MI
    %       PAC_sliding     - structure with calculated PLV,MVL and MI in
    %                         sliding window

    channel_name = removeInvalidStructureCharacters(channel_name);
    %Initialize PAC as an empty struct if it does not exist
    if ~exist('PAC', 'var')
        PAC = struct;
    end
    if ~exist('PAC_sliding', 'var')
        PAC_sliding = struct;
    end

    for i = 1:size(T,1)
        id_st = round(T(i,1)*fs - floor(t(1)*fs));
        id_end = round(T(i,2)*fs- ceil(t(1)*fs));
        if id_st == 0
            id_st = 1;
        elseif id_st < 0
            error('Error: start time of interval must be grater value than zero')
        elseif id_end < 0
            error('Error: end time of interval must be grater value than zero')
        elseif id_end <= id_st
            error('Error: start time must be smaller value than end time')
        end
        if id_st < 1
            id_st = 1;
        end

        if id_end > length(signal)
            id_end = length(signal);
        end
        s_slice = signal(id_st:id_end);
        t_slice = t(id_st:id_end);

        sgtit_time= [T(i,1) T(i,2)];
        plot_set = 1;
        [PLV,MVL,MI] = GPT_MI_KL_D(s_slice, fs, phase_freq_band, Amp_freq_band, n_bins,sgtit_time,plot_set,channel_name,t_slice);
        tit_name = strcat('time_slice_', num2str(round(sgtit_time(1))), '_', num2str(round(sgtit_time(2))), 's');


        % Initialize fields if they don't exist
        if ~isfield(PAC, tit_name)
            PAC.(tit_name) = struct('PLV', [], 'MVL', [], 'MI', []);
        end
        PAC.(tit_name).PLV = PLV;
        PAC.(tit_name).MVL = MVL;
        PAC.(tit_name).MI = MI;

        minWindowSize = windowSize*0.5;
        [windowedSignal,windowedTime] = slidin_win(s_slice,t_slice,windowSize,overlap,minWindowSize);
        plot_set = 0;
        PLVslidin = zeros(size(windowedSignal,2),1);
        MVLslidin = zeros(size(windowedSignal,2),1);
        MIslidin = zeros(size(windowedSignal,2),1);
        for j = 1:size(windowedSignal,2)
            sig_win = windowedSignal{j};
            t_win = windowedTime{j};

            [PLV,MVL,MI] = GPT_MI_KL_D(sig_win, fs, phase_freq_band, Amp_freq_band, n_bins,sgtit_time,plot_set,channel_name);
            PLVslidin(j) = PLV; MVLslidin(j) = MVL; MIslidin(j) = MI; 
        end
        if ~isfield(PAC, tit_name)
            PAC_sliding.(tit_name) = struct('PLVsliding', [], 'MVLsliding', [], 'MIsliding', []);
        end
        PAC_sliding.(tit_name).PLVsliding = PLVslidin;
        PAC_sliding.(tit_name).MVLsliding = MVLslidin;
        PAC_sliding.(tit_name).MIsliding = MIslidin;

        tit_name = strcat('PAC in sliding windows time slice: ',num2str(sgtit_time(1)), '-',num2str(sgtit_time(2)),'s');
        t_slidin = linspace(t_slice(1),t_slice(end),length(PLVslidin));
        hold off
        figure
        subplot(311)
        plot(t_slidin,PLVslidin)
        title('PLV')

        subplot(312)
        plot(t_slidin,MVLslidin)
        title('MVL')

        subplot(313)
        plot(t_slidin,MIslidin)
        title('MI')
        sgtitle(strcat(tit_name,' of signal ',channel_name))

    end
    barChart_plot(PAC,channel_name)
end

function [PLV,MVL,MI] = GPT_MI_KL_D(original_signal, fs, f_low, f_high, n_bins,sgtit_time,plot_set,channel_name,t)
    % main fucntion for calculation phase-locking value, mean vector length
    % and modulation index
    % input:
    %       original_signal - signal for calculation
    %       fs              - sampling frequency of original_signal
    %       f_low           - array [a b] for phase carrying freq band 
    %       f_high          - array [a b] for amplitude carrying freq band
    %       n_bins          - number of bins for MI calculation (18 is commonly used)
    %       sgtit_time      - time range of interest
    %       plot_set        - if plot_set==1 -> show plots
    %       channel_name    - channel names
    


    % set maximal Freq of interest
    F_h = f_high(2) + 50;
    % Control dimension of signal vector
    if size(original_signal,1)>1
        original_signal = original_signal';
    end

    % Filter 50Hz and its harmonics
    original_signal = notch_eeg(original_signal, fs,F_h);


    % Design bandpass filters for the low-frequency and high-frequency bands
    lpFilt = designfilt('bandpassiir','FilterOrder',20,'HalfPowerFrequency1',f_low(1),'HalfPowerFrequency2',f_low(2),'SampleRate',fs);
    hpFilt = designfilt('bandpassiir','FilterOrder',20,'HalfPowerFrequency1',f_high(1),'HalfPowerFrequency2',f_high(2),'SampleRate',fs);


    low_freq_signal = filtfilt(lpFilt, original_signal);
    high_freq_signal = filtfilt(hpFilt, original_signal);

    % Apply the Hilbert transform to calculate instantaneous phase and amplitude
    phase = angle(hilbert(low_freq_signal));
    amplitude = abs(hilbert(high_freq_signal));
    %% PLV
    [PLV,plv_e] = phase_locking_value(phase, amplitude);
    plv_vec = mean(plv_e);

%% MVL

    MVL_e = amplitude .* exp(1i * phase);

    MVL_vec = 1/length(MVL_e) * sum(MVL_e);
    MVL = abs(MVL_vec);

%     MVL = abs(mean(amplitude .* exp(1i * phase)));
%% MI
    % Bin the phase
    bin_edges = linspace(-pi, pi, n_bins+1);
    [~,~,bin_idx] = histcounts(phase, bin_edges);
    p_mean = (bin_edges(1:end-1) + bin_edges(2:end)) / 2;
    % Average the amplitude in each phase bin
    mean_amp = accumarray(bin_idx', amplitude, [n_bins 1], @mean);

    % Normalize the mean amplitudes
    mean_amp = (mean_amp / sum(mean_amp));

    % Calculate the Modulation Index
    H = -sum(mean_amp.*log(mean_amp));
    KL_GPT = (log(length(mean_amp)) - H);
    MI =  KL_GPT / log(length(mean_amp));
%     [MI_old,p_mean_old,a_mean_old] = modulation_index(phase, amplitude, n_bins);
    
    %% PLOTS
    if plot_set == 1
    if size(p_mean,1) > 1
        p_mean = p_mean';
    elseif size(mean_amp,1) > 1
        mean_amp = mean_amp';
    end
    bns = n_bins;

    % PLV
    figure
    subplot(231)
    h = compass(real(plv_vec), imag(plv_vec));
    title('Phase-Locking Value')
    % get position data for the arrow
    x = get(h, 'XData'); y = get(h, 'YData');
    % place text at the end of the arrow
    text(x(end), y(end), num2str(abs(plv_vec)), 'VerticalAlignment', 'bottom', 'HorizontalAlignment', 'right');

    subplot(234)
    polarhistogram(angle(plv_e),bns)
    title('Phase-Locking Value')

    %MVL
    subplot(232)
    h = compass(real(MVL_vec),imag(MVL_vec));
    title('Mean Vector Length')
    % get position data for the arrow
    x = get(h, 'XData'); y = get(h, 'YData');
    % place text at the end of the arrow
    text(x(end), y(end), num2str(abs(MVL_vec)), 'VerticalAlignment', 'bottom', 'HorizontalAlignment', 'right');

    subplot(235)
    polarplot(angle(MVL_e),abs(MVL_e),'x')
    title('Mean Vector Length')

    %MI
    subplot(233)
    bar(rad2deg(p_mean),mean_amp,"BarWidth",1)
    ylabel('High-frequency amplitude'); xlabel('Low-frequency phase [deg]')
    xlim([-180 180])
    grid on
    title('Phase - Amplitude dependency')

    subplot(236)
    [r_MI,i_MI]=pol2cart(p_mean,mean_amp);
    compass(real(r_MI), i_MI)
    title('Phase - Amplitude dependency')
    tit_name = strcat(' time slice: ',num2str(sgtit_time(1)), '-',num2str(sgtit_time(2)),'s');
    sgtitle(strcat(tit_name,' of signal: ',channel_name))
    end

end


function eeg_data = notch_eeg(eeg_data, fs,F_h)
    
    max_har = floor(((F_h-1))/50);
    F_filt = 50;
    for i = 1:max_har
        F_filt_actual = F_filt*i;
        d = designfilt('bandstopiir','FilterOrder',20, ...
                   'HalfPowerFrequency1',F_filt_actual-1,'HalfPowerFrequency2',F_filt_actual+1, ...
                   'DesignMethod','butter','SampleRate',fs);
        eeg_data = filtfilt(d,eeg_data);
         
       
    end
end

function [plv,plv_e] = phase_locking_value(phase_low, amp_high)
    % Phase angles of amplitude envelope of fast F band
    phase_high = angle(hilbert(amp_high));

    % phase angle differences
    phase_diff = phase_low - phase_high;

%     phase_diff = angle(low_freq_analytic) - angle(high_freq_analytic);

    % Compute the Phase Locking Value
    plv_e = exp(1i * phase_diff);
    plv = abs(mean(exp(1i * phase_diff)));



end



function [windowedSignal,windowedTime] = slidin_win(signal,t,windowSize,overlap,minWindowSize)
    overlap = round(windowSize * overlap);
    
    % Calculate step size
    stepSize = windowSize - overlap;
    
    % Calculate number of windows
    numWindows = floor((length(signal) - overlap) / stepSize);
    
    % Initialize windowed signal and time
    windowedSignal = cell(1, numWindows);
    windowedTime = cell(1, numWindows);
    
    % Perform sliding window with overlap
    for i = 1:numWindows
        % Calculate indices for signal
        startIdx = (i-1)*stepSize + 1;
        endIdx = startIdx + windowSize - 1;
    
        % Save windowed signal and time
        windowedSignal{i} = signal(startIdx:endIdx);
        windowedTime{i} = t(startIdx:endIdx);
    end
    
    % Check if the last window is larger than the minimum window size and include it
    lastWindowSize = round(length(signal) - (numWindows * stepSize) + overlap);
    if lastWindowSize >= minWindowSize && lastWindowSize <= length(signal)
        startIdx = max(1, length(signal) - lastWindowSize + 1); % Make sure the start index is at least 1
        windowedSignal{numWindows + 1} = signal(startIdx:end);
        windowedTime{numWindows + 1} = t(startIdx:end);
    end
end


function barChart_plot(PAC,channel_name)
% Get field names of the PAC structure
fields = fieldnames(PAC);

% Initialize arrays to hold PLV, MVL, and MI values
PLVs = zeros(1, length(fields));
MVLs = zeros(1, length(fields));
MIs = zeros(1, length(fields));

% Initialize cell array to hold time ranges
timeRanges = strings(1, length(fields)); 
if size(fields,1) > 1
% Loop over each field in PAC to access PLV, MVL, and MI
    for i = 1:length(fields)
        PLVs(i) = PAC.(fields{i}).PLV;
        MVLs(i) = PAC.(fields{i}).MVL;
        MIs(i) = PAC.(fields{i}).MI;
        
        % Extract time range from field name and save it
        timeRange = extractBetween(fields{i}, 'slice_', 's');
        
        if ~isempty(timeRange)
            timeRanges(i) = replace(timeRange, '_', '-');
        else
            timeRanges(i) = "unknown";
        end
    end


    % Get unique time ranges and assign a color to each one
    uniqueTimeRanges = unique(timeRanges);
    colors = {'r', 'g', 'b', 'c', 'm', 'y', 'k'}; % Extend this list if you have more than 7 time ranges
    colorMap = containers.Map(uniqueTimeRanges, colors(1:length(uniqueTimeRanges)));
    
    % Create bar charts in subplots
    figure;
    
    subplot(1, 3, 1);
    hold on; % Allows multiple bars to be drawn on the same plot
    for i = 1:length(PLVs)
        bar(i, PLVs(i), colorMap(timeRanges(i))); 
    end
    hold off;
    title('PLV');
    set(gca, 'XTickLabel',timeRanges, 'XTick',1:numel(timeRanges))
    
    subplot(1, 3, 2);
    hold on;
    for i = 1:length(MVLs)
        bar(i, MVLs(i), colorMap(timeRanges(i))); 
    end
    hold off;
    title('MVL');
    set(gca, 'XTickLabel',timeRanges, 'XTick',1:numel(timeRanges))
    
    subplot(1, 3, 3);
    hold on;
    for i = 1:length(MIs)
        bar(i, MIs(i), colorMap(timeRanges(i))); 
    end
    hold off;
    title('MI');
    set(gca, 'XTickLabel',timeRanges, 'XTick',1:numel(timeRanges))
    sgtitle(strcat('PAC values of signal: ',channel_name))
end
end

function newStr = removeInvalidStructureCharacters(str)
    % Invalid characters for structure names
    invalidCharacters = ['_'];
    
    % Remove invalid characters from the string
    newStr = str;
    for i = 1:length(invalidCharacters)
        newStr = strrep(newStr, invalidCharacters(i), ' ');
    end
end

