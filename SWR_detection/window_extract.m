function WINS = window_extract(centerIdx, signal, fs, windowSize_ms)
    
    for i = 1:length(centerIdx)
        
    % Convert window size to samples
        windowSize_samples = round((windowSize_ms / 1000) * fs);
        halfWindow = windowSize_samples / 2;
        
        % Calculate start and end indices for the window
        startIdx = max(centerIdx(i) - floor(halfWindow), 1);
        endIdx = min(centerIdx(i) + ceil(halfWindow) - 1, length(signal));
        
        % Extract window
        window = signal(startIdx:endIdx);
        
        % Handle edge cases: pad with zeros if window is shorter
        if length(window) < windowSize_samples
            window = [window, zeros(1, windowSize_samples - length(window))];
        end
        WINS(i,:) = window;
    end
end