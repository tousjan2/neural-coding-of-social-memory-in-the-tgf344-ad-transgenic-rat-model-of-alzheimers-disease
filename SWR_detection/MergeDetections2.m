function mergedIndices = MergeDetections2(v1,v2,fs)
% Given vectors: v1, v2
% Given sampling frequency: fs

% Convert indices to time (ms)
t1 = (v1 - 1) * (1000 / fs);
t2 = (v2 - 1) * (1000 / fs);


% Merge and sort
mergedTimes = sort([t1, t2]);

% Merge close-by events (within 100ms)
i = 1;
while i < length(mergedTimes)
    j = i + 1;
    while j <= length(mergedTimes) && (mergedTimes(j) - mergedTimes(i)) <= 100
        j = j + 1;
    end
    
    % Take average time as representative for the merged events
    mergedTimes(i) = mean(mergedTimes(i:j-1));
    
    % Remove merged times
    mergedTimes(i+1:j-1) = [];
    
    i = i + 1;
end

% Convert back to indices if needed
mergedIndices = round(mergedTimes * (fs / 1000) + 1);
end