function id_new_WV = final_detector(signal,fs,HIL_thr,SLL_thr,freqBand,Filter_window,epochLength)


%% HIL
minWind = 10;
rippleCenters_HIL = detectRipplesHilbert(signal',freqBand, HIL_thr, minWind,epochLength , fs);

%% SLL
min_win = 10e-3;
    
[rippleCenters_SLL] = SLL_LAB(signal', fs, freqBand,Filter_window,epochLength,SLL_thr,min_win);    
%% Meerge detections
mergedIndices = MergeDetections2(rippleCenters_HIL',rippleCenters_SLL',fs);


%%
windowSize_ms = 100;
windows = window_extract(mergedIndices, signal, fs, windowSize_ms);

%% Find threshhold for deleting of FP group 

[WV_thrs] = find_representative_hfo(windows,fs);

%% Deleting of FP group

[~,id_new_WV] = delete_WV_max(windows,mergedIndices,fs,WV_thrs);

end

