function [max_mag1] = waveletTransform(signal, Fs)
    
    % Ensure the signal is a column vector
    if size(signal, 1) == 1
        signal = signal';
    end
    
    [coefficients, ~] = cwt(signal, "amor", Fs, 'FrequencyLimits', [100 200]);
    max_mag1 = max(max(abs(coefficients)));

end