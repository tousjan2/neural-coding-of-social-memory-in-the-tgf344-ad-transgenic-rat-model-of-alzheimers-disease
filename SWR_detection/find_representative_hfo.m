function [threshold] = find_representative_hfo(TP,fs)

iter = size(TP,1);
%%
for i = 1:iter
    Powers_TP(i) = waveletTransform(TP(i,:), fs);
end
%%
Powers_TP = Powers_TP(:);
% Fit a Gaussian Mixture Model with 2 components
GMM = fitgmdist(Powers_TP, 2);

% The threshold can be the mean of the Gaussian components' means
threshold = mean(GMM.mu);


end










