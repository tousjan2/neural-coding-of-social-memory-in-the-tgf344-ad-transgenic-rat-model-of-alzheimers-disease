function rippleCenters = detectRipplesHilbert(signal,freqBand, SDThres, minWind,epochLength , fs)

% Parameters
minWind = minWind * 1e-3; % Min window time for an HFO (ms)


% 1. Bandpass filter
[filteredSignal] = bp_fil(signal, freqBand, fs);

% 2. Hilbert transform
hilbertSignal = abs(hilbert(filteredSignal));

% 3. Thresholding
epochLength = round(epochLength * fs);
epochTemp = (1:epochLength:length(signal))';
minWind = round(minWind * fs);

if epochTemp(end) < length(signal)
    epochTemp(end+1) = length(signal);
end

epochLims = [epochTemp(1:end-1) epochTemp(2:end)-1];
epochs = size(epochLims,1);
clear epochTemp epochLength

HFOEvents = [];  

for ii = 1:size(epochLims,1)
    epochFilt = hilbertSignal(epochLims(ii,1):epochLims(ii,2));
    winThres = epochFilt > (mean(epochFilt) + SDThres * std(epochFilt));

    if isempty(numel(find(winThres)))
        continue
    end

    windThres = [0; winThres; 0];
    windJumps = diff(windThres);
    windJumUp = find(windJumps == 1);
    windJumDown = find(windJumps == -1) - 1;        
    winDist = windJumDown - windJumUp;

    distSelect = (winDist > minWind);
    windJumUp = windJumUp(distSelect);  
    windJumDown = windJumDown(distSelect) - 1;

    windSelect = [windJumUp windJumDown] + epochLims(ii,1) - 1;

    if any(windSelect(:))
        HFOEvents = vertcat(HFOEvents, windSelect);
    end
end

% Calculate the centers of detected ripples
rippleCenters = mean(HFOEvents, 2);

end

function [signal_filt] = bp_fil(signal, F_c, fs)

    Filt_d = designfilt('bandpassiir','FilterOrder',20,'HalfPowerFrequency1',F_c(1),'HalfPowerFrequency2',F_c(2),'SampleRate',fs);
    signal_filt = filtfilt(Filt_d, signal);


end
