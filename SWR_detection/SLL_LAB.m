function [rippleCenters] = SLL_LAB(signal, fs, freqBand,Filter_window,Epoch_win,percentile,min_win)

% Parameters
Filter_window = round((Filter_window)*fs);
percentile = percentile*1e-2;


% 1. Bandpass filter
filteredSignal = bp_fil(signal, freqBand, fs);

% 2. Energy Line Length
filteredSignal = [filteredSignal(1); filteredSignal];
temp = abs(diff(filteredSignal));
temp = filter(ones(1, Filter_window), 1, temp) ./ Filter_window;
energy = zeros(numel(temp), 1);
energy(1:end - ceil(Filter_window / 2) + 1) = 10 * temp(ceil(Filter_window / 2):end);

% 3. Thresholding
min_win = round(min_win * fs);
Epoch_win = round(Epoch_win .* fs);
epochs = round(numel(energy) / Epoch_win);
threshold = zeros(numel(energy), 1);

for kk = 1:epochs
    ini = floor((kk-1) .* Epoch_win) + 1;
    fin = ini + Epoch_win;
    if fin > numel(energy)
        fin = numel(energy);
    end
    
    perc = edfcnd(energy(ini:fin), -inf, [], 'method', 3);
    val = perc(:, 1);
    perc = perc(:, 2);
    
    idx = find(perc <= percentile, 1, 'last');
    threshold(ini:fin) = val(idx);
end

energyThres = energy >= threshold;

% 4. Interval Selection
windThres = [0; energyThres; 0];
windJumps = diff(windThres);
windJumUp = find(windJumps == 1);
windJumDown = find(windJumps == -1);
winDist = windJumDown - windJumUp;

winDistSelect = (winDist > min_win);
windSelect = find(winDistSelect);

if isempty(windSelect)
    HFOEvents = [];
else
    HFOEvents = [windJumUp(windSelect) windJumDown(windSelect)-1];
end

% Calculate the centers of detected ripples
rippleCenters = mean(HFOEvents, 2);

end

function [F,G] = edfcnd(z,c,G,varargin) 
                
z = sort(z);
I = find(z>=c);

z = z(I);
N = length(z);

Fz1 = (1:N)'/N;

Fz = [z(:),Fz1(:)];
F = Fz;
end

function [signal_filt] = bp_fil(signal, F_c, fs)

    Filt_d = designfilt('bandpassiir','FilterOrder',20,'HalfPowerFrequency1',F_c(1),'HalfPowerFrequency2',F_c(2),'SampleRate',fs);
    signal_filt = filtfilt(Filt_d, signal);


end
