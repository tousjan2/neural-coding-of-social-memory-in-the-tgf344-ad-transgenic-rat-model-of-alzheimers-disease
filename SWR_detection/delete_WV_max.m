function [WIN_NEW,id_new] = delete_WV_max(WINDOWS,indxes,fs,thrs)
cnt = 1;
for i = 1:size(WINDOWS,1)
    s_TP = WINDOWS(i,:);
    [max_mag1] = waveletTransform(s_TP, fs);

    

    if max_mag1 >= thrs
        
        WIN_NEW(cnt,:) = WINDOWS(i,:);
        id_new(cnt) = indxes(i);
        cnt = cnt+1;

    end

end



end